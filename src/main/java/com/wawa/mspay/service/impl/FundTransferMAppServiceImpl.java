package com.wawa.mspay.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import com.wawa.mspay.client.WawaBankFundTransferClient;
import com.wawa.mspay.dto.StatementDetailsDto;
import com.wawa.mspay.entity.StatementDetails;
import com.wawa.mspay.entity.UserInfo;
import com.wawa.mspay.repository.FundTransferMAppRepository;
import com.wawa.mspay.repository.UserRegisterMAppRepository;
import com.wawa.mspay.service.FundTransferMAppService;

@Service
public class FundTransferMAppServiceImpl implements FundTransferMAppService {
	
	@Autowired
	FundTransferMAppRepository fundTransferMAppRepository;
	
	@Autowired
	WawaBankFundTransferClient bankFundTransferClient;
	
	@Autowired
	UserRegisterMAppRepository  userMAppRepository;

	@Override
	public String fundTransferByMobileNo(Long sourceMobileNo, Long destMobileNo, double amount) throws Exception {
		
		UserInfo sourceUserInfo= userMAppRepository.findByMobileno(sourceMobileNo);
	    UserInfo destUserInfo= userMAppRepository.findByMobileno(destMobileNo);
	    if (sourceUserInfo == null) {
	    	throw new Exception("Source_User's mobileNo not registered");
	    }
	    if (destUserInfo == null) {
	    	throw new Exception("Destination_User's mobileNo not registered");
	    }	
		
		
		ResponseEntity<String>  responseType =bankFundTransferClient.getfundTransferByMobileNo(sourceMobileNo, destMobileNo, amount);
	       if(responseType.getStatusCode() ==  HttpStatus.OK) {
	    	   StatementDetails statementDetail=new StatementDetails();
	    	   statementDetail.setSourcemobileno(sourceMobileNo);
	    	   statementDetail.setDestmobileno(destMobileNo);
	    	   statementDetail.setAmount(amount);
	    	   statementDetail.setRequestedDate(Timestamp.valueOf(LocalDateTime.now()));
	    	   fundTransferMAppRepository.save(statementDetail);
	       }
		
		return  responseType.getBody();
	}

	@Override
	public List<StatementDetailsDto> getStatementReport(Long mobileNumber) {
		
		List<StatementDetails> listStatementDetails=fundTransferMAppRepository.findTop10ByDestmobilenoOrSourcemobilenoOrderByRequestedDateDesc(mobileNumber,mobileNumber);
		List<StatementDetailsDto> listStatementDetailsDto=listStatementDetails.stream().map(sta-> new StatementDetailsDto(sta.getSourcemobileno(),sta.getDestmobileno(),sta.getAmount(),
				sta.getRequestedDate())).collect(Collectors.toList());
		return listStatementDetailsDto;
	}

}
