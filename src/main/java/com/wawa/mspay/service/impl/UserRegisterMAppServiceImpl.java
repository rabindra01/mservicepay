package com.wawa.mspay.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wawa.mspay.client.WawaBankFundTransferClient;
import com.wawa.mspay.dto.UserDto;
import com.wawa.mspay.entity.UserInfo;
import com.wawa.mspay.repository.UserRegisterMAppRepository;
import com.wawa.mspay.service.UserRegisterMAppService;

@Service
public class UserRegisterMAppServiceImpl implements UserRegisterMAppService {
	
	@Autowired
	UserRegisterMAppRepository  userMAppRepository;
	
	
	@Autowired
	WawaBankFundTransferClient bankFundTransferClient;
	
	@Override
	public String createUseRegister(UserDto userDto) {
		String retValue = "Mobileno isn't register in bank";
		
		ResponseEntity<Boolean>  response= bankFundTransferClient.checkMobileNumber(userDto.getMobileno());
		
		if(response.hasBody()) {
		UserInfo userInfo=new UserInfo();
		BeanUtils.copyProperties(userDto, userInfo);
		userMAppRepository.save(userInfo);
		retValue="user is registered";
	   }
		return retValue;
		
	}
}
