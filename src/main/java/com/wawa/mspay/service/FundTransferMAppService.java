package com.wawa.mspay.service;

import java.util.List;

import com.wawa.mspay.dto.StatementDetailsDto;

public interface FundTransferMAppService {

	public String fundTransferByMobileNo(Long sourceMobileNo, Long destMobileNo, double amount) throws Exception;

	public List<StatementDetailsDto> getStatementReport(Long mobileNumber);

}
 