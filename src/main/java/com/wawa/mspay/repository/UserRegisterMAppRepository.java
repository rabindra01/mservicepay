package com.wawa.mspay.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wawa.mspay.entity.UserInfo;

@Repository
public interface UserRegisterMAppRepository extends JpaRepository<UserInfo, Long> {

	public UserInfo findByMobileno(Long mobileNo);

}
