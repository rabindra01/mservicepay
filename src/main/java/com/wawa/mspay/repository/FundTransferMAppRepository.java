package com.wawa.mspay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wawa.mspay.entity.StatementDetails;

@Repository
public interface FundTransferMAppRepository extends JpaRepository<StatementDetails, Long> {

	List<StatementDetails> findTop10ByDestmobilenoOrSourcemobilenoOrderByRequestedDateDesc(
			Long mobileNumber, Long mobileNumber2);

}
