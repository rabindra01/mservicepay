package com.wawa.mspay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wawa.mspay.dto.UserDto;
import com.wawa.mspay.service.UserRegisterMAppService;

@RestController
@RequestMapping("register")
public class UserRegisterMAppController {
	
	
	
	@Autowired
	private UserRegisterMAppService userRegisteService;
		
	
	@PostMapping("/create")
	public ResponseEntity<String> createUseRegister(@RequestBody UserDto userDto) {
		userRegisteService.createUseRegister(userDto);
	    return new ResponseEntity<>("Registered",HttpStatus.CREATED); 
	}

}
