package com.wawa.mspay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wawa.mspay.dto.StatementDetailsDto;
import com.wawa.mspay.service.FundTransferMAppService;

@RestController
@RequestMapping()
public class FundTransferMAppController {
	
	
	@Autowired
	FundTransferMAppService fundTransferMAppService;
	
	
	@PostMapping("/mtransfer")
	public ResponseEntity<String> fundTransferByMobileNo(@RequestParam Long sourceMobileNo, @RequestParam  Long destMobileNo, @RequestParam double amount ) {	
	  String retMessage=null;
		try { 
		    retMessage=fundTransferMAppService.fundTransferByMobileNo(sourceMobileNo, destMobileNo, amount );
	    }catch(Exception e) {
	    	retMessage=e.getMessage();
	 }
		return new ResponseEntity<>(retMessage,HttpStatus.OK);
		
	}
    

	@GetMapping("/statement")
	public  ResponseEntity<List<StatementDetailsDto>> getStatementReport(@RequestParam Long mobileNumber){
		
		List<StatementDetailsDto> listTransDetailsDto=fundTransferMAppService.getStatementReport(mobileNumber);
		return new ResponseEntity<>(listTransDetailsDto,HttpStatus.OK);
	}
}
