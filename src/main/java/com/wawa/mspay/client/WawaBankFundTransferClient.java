package com.wawa.mspay.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="http://bank-service/bank")
public interface WawaBankFundTransferClient {
	
	
	@GetMapping("/users/checkregister")
	public ResponseEntity<Boolean> checkMobileNumber(@RequestParam Long mobilenumber);
	
	@PostMapping("/fundtrans/mstransfer")
	public ResponseEntity<String> getfundTransferByMobileNo(@RequestParam Long sourceMobileNo, @RequestParam  Long destMobileNo, @RequestParam double amount );

}
