package com.wawa.mspay.dto;

import java.io.Serializable;

public class UserDto implements Serializable{
	
	
/**
	 * 
	 */
private static final long serialVersionUID = 1L;
	

private Long 	mobileno;	

private String 	firstname;	
	
private String 	lastname;	

private Integer 	age;

public Long getMobileno() {
	return mobileno;
}

public void setMobileno(Long mobileno) {
	this.mobileno = mobileno;
}

public String getFirstname() {
	return firstname;
}

public void setFirstname(String firstname) {
	this.firstname = firstname;
}

public String getLastname() {
	return lastname;
}

public void setLastname(String lastname) {
	this.lastname = lastname;
}

public Integer getAge() {
	return age;
}

public void setAge(Integer age) {
	this.age = age;
}




}
