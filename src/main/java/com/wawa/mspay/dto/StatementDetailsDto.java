package com.wawa.mspay.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


public class StatementDetailsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Long	sourcemobileno;	
	
	private Long	destmobileno;	
	
	private double	amount;	
	
	private Date	requestedDate;

	public StatementDetailsDto() {
		super();
	}

	public StatementDetailsDto( Long sourcemobileno, Long destmobileno, double amount,
			Date requestedDate) {
		super();
		this.sourcemobileno = sourcemobileno;
		this.destmobileno = destmobileno;
		this.amount = amount;
		this.requestedDate = requestedDate;
	}

	private static final SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	
	public Long getSourcemobileno() {
		return sourcemobileno;
	}

	public void setSourcemobileno(Long sourcemobileno) {
		this.sourcemobileno = sourcemobileno;
	}

	public Long getDestmobileno() {
		return destmobileno;
	}

	public void setDestmobileno(Long destmobileno) {
		this.destmobileno = destmobileno;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRequestedDate() {
		return  sdf.format(requestedDate);
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

}
