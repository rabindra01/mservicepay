package com.wawa.mspay.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "STATEMENT_DETAILS")
public class StatementDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "statement_Id")
	private Long   statement_Id;
	
	@Column(name = "source_mobileno")
	private Long	sourcemobileno;	
	
	@Column(name = "dest_mobileno")
	private Long	destmobileno;	
	
	private double	amount;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "requested_date")
	private Date	requestedDate;
	
	

	public Long getStatement_Id() {
		return statement_Id;
	}

	public void setStatement_Id(Long statement_Id) {
		this.statement_Id = statement_Id;
	}

	public Long getSourcemobileno() {
		return sourcemobileno;
	}

	public void setSourcemobileno(Long sourcemobileno) {
		this.sourcemobileno = sourcemobileno;
	}

	public Long getDestmobileno() {
		return destmobileno;
	}

	public void setDestmobileno(Long destmobileno) {
		this.destmobileno = destmobileno;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

}
